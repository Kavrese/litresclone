package com.example.litresclone.Fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.litresclone.Adapters.AdapterRecMain
import com.example.litresclone.Classes.Codes
import com.example.litresclone.Classes.InitsData
import com.example.litresclone.Internet.ModelBook
import com.example.litresclone.Internet.ModelCategory
import com.example.litresclone.R
import kotlinx.android.synthetic.main.fragment_books.*

class BooksFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rec_books.apply {
            layoutManager = LinearLayoutManager(requireContext())
        }
        Codes().initCategories(object : InitsData {
            override fun initCategories(listCategories: List<ModelCategory>){
                rec_books.adapter = AdapterRecMain(listCategories, R.layout.item_book)
            }

            override fun initBook(book: ModelBook){
                TODO("Not yet implemented")
            }
        }, requireContext())
    }
}