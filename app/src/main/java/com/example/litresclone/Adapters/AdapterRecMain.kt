package com.example.litresclone.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.litresclone.Internet.ModelCategory
import com.example.litresclone.R

class AdapterRecMain(private val listCategory: List<ModelCategory>, val idItem: Int): RecyclerView.Adapter<AdapterRecMain.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name_category = itemView.findViewById<TextView>(R.id.text_categories)
        val rec_category = itemView.findViewById<RecyclerView>(R.id.rec_categories)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rec_main, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name_category.text = listCategory[position].name
        holder.rec_category.apply {
            adapter = AdapterCategoryRec(listCategory[position].list_book, idItem)
            layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }
    }

    override fun getItemCount(): Int = listCategory.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}