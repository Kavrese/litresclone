package com.example.litresclone.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.litresclone.*
import com.example.litresclone.Classes.Codes
import com.example.litresclone.Classes.InitsData
import com.example.litresclone.Internet.ModelBook
import com.example.litresclone.Internet.ModelCategory

class AdapterCategoryRec(private val list_book: List<String>, private val idItem: Int): RecyclerView.Adapter<AdapterCategoryRec.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name_book = itemView.findViewById<TextView>(R.id.name)
        val author = itemView.findViewById<TextView>(R.id.author)
        val stars = itemView.findViewById<TextView>(R.id.stars)
        val cover_book = itemView.findViewById<ImageView>(R.id.cover_book)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(idItem, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        FirebaseDatabase.getInstance().reference.child("Books").child(list_book[position]).addListenerForSingleValueEvent(object : ValueEventListener{
//                override fun onDataChange(snapshot: DataSnapshot) {
//                    val model = snapshot.getValue(ModelBook::class.java)
//                    if (model != null) {
//                        holder.name_book.text = model.name_book
//                        holder.author.text = model.author
//                        holder.type.text = "ТЕКСТ"
//                        holder.stars.text = model.rating.toString()
//                        Glide.with(holder.itemView.context)
//                            .load(model.cover)
//                            .into(holder.cover_book)
//                    }
//                }
//
//                override fun onCancelled(error: DatabaseError) {
//                    Toast.makeText(holder.itemView.context, "Книга не найдена ${list_book[position]}", Toast.LENGTH_SHORT).show()
//                }
//            })
        if (idItem == R.layout.item_book) {
            Codes().initBook(object : InitsData {
                override fun initCategories(listCategories: List<ModelCategory>) {
                    TODO("Not yet implemented")
                }

                override fun initBook(book: ModelBook) {
                    initBook(holder, book)
                }
            }, holder.itemView.context, list_book[position])
        }else{
            Codes().initAudioBook(object : InitsData {
                override fun initCategories(listCategories: List<ModelCategory>) {
                    TODO("Not yet implemented")
                }

                override fun initBook(book: ModelBook) {
                    initBook(holder, book)
                }
            }, holder.itemView.context, list_book[position])
        }
    }

    private fun initBook(holder: ViewHolder, book: ModelBook){
        holder.name_book.text = book.name_book
        holder.author.text = book.author
        holder.stars.text = book.rating.toString()
        Glide.with(holder.itemView.context)
            .load(book.cover)
            .into(holder.cover_book)
    }

    override fun getItemCount(): Int = list_book.size

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}