package com.example.litresclone.Activites

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.litresclone.Adapters.AdapterViewPager
import com.example.litresclone.Fragments.AudioBooksFragment
import com.example.litresclone.Fragments.BooksFragment
import com.example.litresclone.Fragments.WhatReadFragment
import com.example.litresclone.R
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val list_fragments = listOf<Fragment>(WhatReadFragment(), BooksFragment(), AudioBooksFragment())

        viewpager.apply {
            adapter = AdapterViewPager(this@MainActivity, list_fragments)
        }

        val tabs = TabLayoutMediator(tab, viewpager
        ) { tab, position ->
            tab.text = arrayListOf("ЧТО ПОЧИТАТЬ?", "КНИГИ", "АУДИОКНИГИ")[position]
        }
        tabs.attach()
    }
}