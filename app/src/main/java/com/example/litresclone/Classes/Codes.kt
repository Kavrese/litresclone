package com.example.litresclone.Classes

import android.content.Context
import android.widget.Toast
import com.example.litresclone.Internet.Api
import com.example.litresclone.Internet.ModelBook
import com.example.litresclone.Internet.ModelCategory
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Codes {
    fun initRetrofit(): Retrofit{
        return Retrofit.Builder()
            .baseUrl("https://enterwork-2bdb8.firebaseio.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun showError(context: Context, t: Throwable){
        Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
    }

    fun showMessage(context: Context, mes: String){
        Toast.makeText(context, mes, Toast.LENGTH_LONG).show()
    }

    fun initCategories(interfaceData: InitsData, context: Context){
        Codes().initRetrofit().create(Api::class.java).getCategories().enqueue(object:
            Callback<List<ModelCategory>> {
            override fun onResponse(
                call: Call<List<ModelCategory>>,
                response: Response<List<ModelCategory>>
            ) {
                if (response.body() != null){
                    interfaceData.initCategories(response.body()!!)
                }else{
                    Codes().showMessage(context, "Error NULL body CATEGORIES")
                }
            }

            override fun onFailure(call: Call<List<ModelCategory>>, t: Throwable) {
                Codes().showError(context, t)
            }

        })
    }

    fun initBook(interfaceData: InitsData, context: Context, idBook: String){
        Codes().initRetrofit().create(Api::class.java).getBook(idBook).enqueue(object: Callback<ModelBook>{
            override fun onResponse(call: Call<ModelBook>, response: Response<ModelBook>) {
                if (response.body() != null){
                    interfaceData.initBook(response.body()!!)
                }else{
                    Codes().showMessage(context, "Error NULL body BOOK")
                }
            }

            override fun onFailure(call: Call<ModelBook>, t: Throwable) {
                Codes().showError(context, t)
            }
        })
    }

    fun initAudioBook(interfaceData: InitsData, context: Context, idBook: String){
        Codes().initRetrofit().create(Api::class.java).getAudioBook(idBook).enqueue(object: Callback<ModelBook>{
            override fun onResponse(call: Call<ModelBook>, response: Response<ModelBook>) {
                if (response.body() != null){
                    interfaceData.initBook(response.body()!!)
                }else{
                    Codes().showMessage(context, "Error NULL body BOOK")
                }
            }

            override fun onFailure(call: Call<ModelBook>, t: Throwable) {
                Codes().showError(context, t)
            }
        })
    }
}