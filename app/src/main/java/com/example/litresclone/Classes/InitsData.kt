package com.example.litresclone.Classes

import com.example.litresclone.Internet.ModelBook
import com.example.litresclone.Internet.ModelCategory

interface InitsData {
    fun initCategories(listCategories: List<ModelCategory>)
    fun initBook(book: ModelBook)
}