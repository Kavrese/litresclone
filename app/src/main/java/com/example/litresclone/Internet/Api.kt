package com.example.litresclone.Internet

import com.example.litresclone.Internet.ModelBook
import com.example.litresclone.Internet.ModelCategory
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    @GET("Books/{idBook}.json")
    fun getBook(@Path("idBook") idBook: String): Call<ModelBook>

    @GET("Categories.json")
    fun getCategories(): Call<List<ModelCategory>>

    @GET("AudioBooks/{idBook}.json")
    fun getAudioBook(@Path("idBook") idBook: String): Call<ModelBook>
}