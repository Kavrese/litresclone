package com.example.litresclone.Internet

import java.time.Duration

data class ModelBook (
    val age: String,
    val author: String,
    val count_page: Int,
    val duration: String,
    val cover: String,
    val language: String,
    val name_book: String,
    val rating: Float,
    val year_global: String,
    val tags: List<String>
)