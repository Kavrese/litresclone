package com.example.litresclone.Internet

data class ModelCategory(
        val name: String,
        val list_book: List<String>
)
